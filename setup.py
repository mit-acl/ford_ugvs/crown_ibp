from setuptools import setup

setup(
    name="crown_ibp",
    version="1.0.0",
    install_requires=[
        "torch",
        "tensorflow-cpu",
    ],
    packages=["crown_ibp", "crown_ibp.conversions"],
)
